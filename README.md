# Flight Controller

Flight control software for multi-rotor aircraft.

## Installation

Within a particular ecosystem, there may be a common way of installing things,
such as using Yarn, NuGet, or Homebrew. However, consider the possibility that
whoever is reading your README is a novice and would like more guidance. Listing
specific steps helps remove ambiguity and gets people to using your project as
quickly as possible. If it only runs in a specific context like a particular
programming language version or operating system or has dependencies that have
to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to
have inline the smallest example of usage that you can demonstrate, while
providing links to more sophisticated examples if they are too long to
reasonably include in the README.

## Support

For issues building or using the software, submit an issue in the [GitLab Issue
Tracker][].

## Contributing

State if you are open to contributions and what your requirements are for
accepting them.

For people who want to make changes to your project, it's helpful to have some
documentation on how to get started. Perhaps there is a script that they should
run or some environment variables that they need to set. Make these steps
explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help
to ensure high code quality and reduce the likelihood that the changes
inadvertently break something. Having instructions for running tests is
especially helpful if it requires external setup, such as starting a Selenium
server for testing in a browser.

## License

Most of the source files for this project are licensed under [GPLv3][]. Some build
and metadata files are licensed under the [MIT][] license.

[GPLv3]: https://opensource.org/license/gpl-3-0/
[MIT]: https://opensource.org/license/mit/
[GitLab Issue Tracker]: https://gitlab.com/quadcopter3973848/flight-controller/-/issues

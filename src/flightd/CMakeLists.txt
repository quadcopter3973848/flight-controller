# SPDX-License-Identifier: MIT
#
# Copyright 2023 Jason Carrete
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the “Software”), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

set(TARGET flightd)
add_executable("${TARGET}")
target_sources(
  "${TARGET}"
  PRIVATE
    main.cpp
    "${CMAKE_CURRENT_BINARY_DIR}/appinfo.cpp"
  PRIVATE
    FILE_SET HEADERS
      FILES appinfo.h
)
target_compile_features(
  "${TARGET}"
  PRIVATE cxx_std_20
)
target_add_warnings("${TARGET}")
target_link_libraries(
  "${TARGET}"
  PRIVATE
    freeflight
)

configure_file(appinfo.cpp.in appinfo.cpp @ONLY)

install(TARGETS "${TARGET}")
